import time
import math

def primeCalc_fastPrimes(upper = 100):
    potentialList = [*range(2, upper + 1)]
    finishedList = [1]
    maxMultiple = math.sqrt(upper + 1)
    while potentialList:
        primeCheck = potentialList[0]
        finishedList.append(primeCheck)
        potentialList.pop(0)
        if potentialList:
            currentLast = potentialList[-1]
        else:
            continue
        for i in potentialList:
            if i % primeCheck == 0:
                potentialList.remove(i)
        if primeCheck > (maxMultiple):
            finishedList.extend(potentialList)
            break
    return finishedList

def primeCalc_traditionalPrimes(test_target = 100):
    finishedList = [1]
    upper = test_target
    for num in range(0, upper + 1):
       # all prime numbers are greater than 1
       if num > 1:
           for i in range(2, num):
               if (num % i) == 0:
                   break
           else:
               finishedList.append(num)
    return finishedList

def primeCalc_setSpeedTest (test_target, show_lists = False):

    start_time = time.time()
    result1 = primeCalc_fastPrimes(test_target)
    end_time = time.time() - start_time
    print('primeCalc_fastPrimes duration:', str(end_time))
    start_time = time.time()
    result2 = primeCalc_traditionalPrimes(test_target)
    end_time = time.time() - start_time
    print('primeCalc_traditionalPrimes duration:', str(end_time))

    if show_lists == True:
        print('primeCalc_fastPrimes list:', *result1)
        print('primeCalc_traditionalPrimes list:', *result2)


def primeCalc_isPrimeClassic(value):
    if value == 1 or value == 2 or value == 3:
        return True
    maxMultiple = math.ceil(math.sqrt(value + 1))
    for i in range(2, maxMultiple + 1):
        if value % i == 0:
            return False
    return True

def primeCalc_isPrimeFast(value):
    if value == 1 or value == 2 or value == 3:
        return True
    maxMultiple = math.ceil(math.sqrt(value + 1))
    primeset = primeCalc_fastPrimes(maxMultiple)
    for i in primeset:
        if value % i == 0:
            return False
    return True

def primeCalc_valueSpeedTest (test_target):

    checkList = range(1, test_target)
    classicList = []
    geeksList = []
    fastList = []
    classicTime = 0
    geeksTime = 0
    fastTime = 0
    for i in checkList:
        start_time = time.time()
        classicList.append((str(i), str(primeCalc_isPrimeClassic(i))))
        classicTime = classicTime + (time.time() - start_time)
        start_time = time.time()
        geeksList.append((str(i), str(primeCalc_isPrimeGeeksForGeeks(i))))
        geeksTime = geeksTime + (time.time() - start_time)
        start_time = time.time()
        fastList.append((str(i), str(primeCalc_isPrimeFast(i))))
        fastTime = fastTime + (time.time() - start_time)

    print('primeCalc_isPrimeClassic:', str(classicTime), 'and result: ', str(classicList))
    print('primeCalc_isPrimeFast:', str(fastTime), 'and result: ', str(fastList))

# primeCalc_setSpeedTest(100, True)
#primeCalc_valueSpeedTest(25)
