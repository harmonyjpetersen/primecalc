# primeCalc
The purpose of primeCalc is to make the calculation of a list of primes quicker than normal.


## Usage

The real purpose of this file is the `primeCalc_fastPrimes(value)` function.

It generates a list of primes, from 0 to X where X is the value passed in.

for example, to call it, simply call `primeCalc_fastPrimes(300)` to generate all the positive primes up to 300. I created this, as I every method I found for generating a list of primes was painfully slow, and I though I found a way to speed up the math by having the generation of each prime build on the generation of the previous primes. This was a significant success, and although faster methods for generating a group of primes may exist, I am not aware of them. So, to my knowledge, this is the fastest group-of-primes generator to exist.

## Other functions

The other functions in PrimeCalc.py are simply for exploring the math of the method.

`primeCalc_traditionalPrimes(value)` is for generating a prime list in a more traditional method, while `primeCalc_setSpeedTest(value, true/false)` is for comparing the two.

`primeCalc_isPrimeClassic(value)` uses a traditional method for checking if a specific given number is a prime, and `primeCalc_isPrimeFast(value)` makes use of the set generation to make it faster, while `primeCalc_valueSpeedTest(value)` gives a test comparing the two methods.

## Other notes

For both the speed tests, there are commented-out examples at the bottom of the file if you want to run the file directly just to test it out.
